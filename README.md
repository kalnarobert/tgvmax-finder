# TGVMAX finder

This project is essentially a small script to find tgvmax tickets between Lyon and Paris.

## Install

This project uses python2, so if for some reason you don't have it, install it.

then clone the repo

```sh
git clone https://gitlab.com/kalnarobert/tgvmax-finder.git
```

and install the dependencies

```sh
./install-dependencies.sh
```

## Usage

Launch

```sh
python2 tgv-finder.py --help
```

to get the available options and understand how to use it:

```
Usage: tgv-finder.py [options]

Options:
  -h, --help            show this help message and exit
  -d DATE, --date=DATE  date of departure or starting date of period queried
  -p PERIOD, --period=PERIOD
                        choose between day, week or month
  --to=DESTINATION, --direction-to=DESTINATION
                        choose destination (paris or lyon)
```

![screenshot.png](screenshot.png)
