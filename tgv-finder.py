#!/usr/bin/python2
# coding: utf-8
import logging
import datetime

from optparse import OptionParser
import requests
import json
from prettytable import PrettyTable
import pandas

DEBUG = False
TIME = 0

# set up logger
logger = logging.getLogger('root')
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
# SHORTER_FORMAT = "[%(filename)s:%(lineno)s ] %(message)s"
# logging.basicConfig(format=SHORTER_FORMAT)
if DEBUG:
    logger.setLevel(logging.DEBUG)


def query_departure_times(day):
    if direction == 'paris':
        query_parameters = {'refine.date': day, 'dataset': 'tgvmax', 'refine.od_happy_card': 'OUI',
                            'entity': 'PARISRHONE',
                            'refine.origine': 'PARIS (intramuros)', 'refine.destination': 'LYON (gares intramuros)'}
    else:
        query_parameters = {'refine.date': day, 'dataset': 'tgvmax', 'refine.od_happy_card': 'OUI',
                            'entity': 'PARISRHONE',
                            'refine.destination': 'PARIS (intramuros)', 'refine.origine': 'LYON (gares intramuros)'}
    url = "https://data.sncf.com/api/records/1.0/search/"
    response = requests.get(url, params=query_parameters)
    if response.ok:
        data = json.loads(response.content)
        data = sorted(data['records'], key=lambda k: k['fields']['heure_depart'])
        departure_times = list(set(d['fields']['heure_depart'] for d in data))
        departure_times.sort()
        departure_times.sort(key=len)
        return departure_times


def query_for_week(starting_date):
    days = [str(starting_date + datetime.timedelta(days=i)) for i in range(0, 7)]
    pretty_timetable = PrettyTable()
    list_of_departure_times = []
    for day in days:
        departure_times = query_departure_times(day)
        list_of_departure_times.append(departure_times)
        # print list_of_departure_times
    max_number_available_trains = len(max(list_of_departure_times, key=len))
    for i in range(0, len(days)):
        pretty_timetable.add_column(days[i], list_of_departure_times[i] + [""] * (
                max_number_available_trains - len(list_of_departure_times[i])))
    print pretty_timetable


def query_tgvmax(date):
    if day_week_or_month == "day":
        departure_times = query_departure_times(date)
        print "available TGVMax tickets on " + date + " from...to.."
        x = PrettyTable()
        x.add_column(date, departure_times)
        print x
    if day_week_or_month == "week":
        query_for_week(pandas.to_datetime(date).date())
    if day_week_or_month == "month":
        for i in range(0, 5):
            query_for_week(pandas.to_datetime(date).date() + datetime.timedelta(days=(i * 7)))


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-d", "--date", dest="date", help="date of departure or starting date of period queried",
                      metavar="DATE")
    parser.add_option("-p", "--period", dest="period_type", help="choose between day, week or month", metavar="PERIOD")
    parser.add_option("--to", "--direction-to", dest="destination", help="choose destination (paris or lyon)",
                      metavar="DESTINATION")
    (options, args) = parser.parse_args()
    if options.date is not None:
        DATE = options.date
    else:
        DATE = str(datetime.date.today())
    if options.period_type is not None and options.period_type in ['day', 'week', 'month']:
        day_week_or_month = options.period_type
    else:
        day_week_or_month = 'week'
    if options.destination is not None and options.destination in ['paris', 'lyon']:
        direction = options.destination
    else:
        direction = 'paris'
    query_tgvmax(DATE)
